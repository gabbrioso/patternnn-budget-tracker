import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container  } from 'react-bootstrap';
import Router from 'next/router'
import UserContext from '../../UserContext';
import Head from 'next/head'

export default function create() {
    //declare form input states
    let token = ""
    
    const [categoryName, setCategoryName] = useState('');
    const [categoryType, setCategoryType] = useState('');
    const { user, setUser } = useContext(UserContext)

    useEffect(() => {
        token = localStorage.getItem('token')
    },[categoryName, categoryType])
    


    //function for processing creation of a new course
    function createCategory(e) {
        e.preventDefault();

        console.log(`${categoryName} is a category with a type: ${categoryType}.`);

        setCategoryName('');
        setCategoryType('');

        fetch(`https://patternnn-budget-tracker.herokuapp.com/api/users/add-category`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify({
                name: categoryName,
                typeName: categoryType
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(categoryType)
            Router.push('/categories')
        })
    }

    

    return (
        <Container>
            <Head>
                <title>PATTERNNN - Add Category</title>
            </Head>
            <div className="new-form container">
                <Form onSubmit={(e) => createCategory(e)}>
                    <Form.Group controlId="categoryName">
                        <Form.Label className="login-text font-weight-bold">Category Name:</Form.Label>
                        <Form.Control type="text" placeholder="Enter category name" value={categoryName} onChange={e => setCategoryName(e.target.value)} required/>
                    </Form.Group>
                    <Form.Group controlId="categoryType">
                        <Form.Label className="login-text font-weight-bold">Category Type:</Form.Label>
                        <Form.Control as="select" type="text" placeholder="Enter category type" onClick={e => setCategoryType(e.target.value)} required>
                            <option value="Income">Income</option>
                            <option value="Expense">Expense</option>
                        </Form.Control>
                    </Form.Group>
                    {/* <Form.Group controlId="categoryType">
                        <Form.Label>Category Type:</Form.Label>
                        <Form.Control type="text" placeholder="Enter category type" value={categoryType} onChange={e => setCategoryType(e.target.value)} required/>
                    </Form.Group> */}

                    <Button variant="outline-danger" className="login-form-button" type="submit">Add Category</Button>
                </Form>
            </div>
        </Container>
    )
}
