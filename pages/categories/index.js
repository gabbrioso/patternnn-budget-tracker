import { useState, useEffect } from 'react';
import { Table, Alert, Row, Col, Card, Button } from 'react-bootstrap'
import { useContext } from 'react';
import UserContext from '../../UserContext';
import Router from 'next/router';
import Link from 'next/link'
import Head from 'next/head'

import AppHelper from '../../app-helper'




export default function index(){
    const [categories, setCategories] = useState([])

    useEffect(() => {
        let token = localStorage.getItem('token')

        fetch(`https://patternnn-budget-tracker.herokuapp.com/api/users/get-categories`, {
            headers: {
                Authorization: `Bearer ${token}`
            } 
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data)
            if(data._id !== null){
                setCategories(data)
            }else{
                setCategories([])
            }
        })
    },[])
	
    return (
        <React.Fragment>
            <Head>
                <title>PATTERNNN - Categories</title>
            </Head>
           <div className="records-display container">
                <Link href="/categories/new">
                    <Button variant="outline-danger" className="login-form-button">
                        <a role="button">Add a Category</a>
                    </Button>
                </Link>
                {categories.length > 0
                ? <Table bordered hover>
                    <thead>
                        <tr className="login-text">
                            <th>Name</th>
                            <th>Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        {categories.map(category =>{
                            return(
                                <tr key={category._id}>  
                                    <td>{category.name}</td>
                                    <td>{category.type}</td>                                     
                                </tr>
                            )
                        })
                        }
                    </tbody>
                </Table>
                :   <Alert variant="danger">You have no categories yet.</Alert>
                }
            </div>
        </React.Fragment>
    )
}