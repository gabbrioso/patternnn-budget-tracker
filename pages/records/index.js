import { useState, useEffect } from 'react';
import { Row, Col, Card, Button, ListGroup, Form } from 'react-bootstrap'
import { useContext } from 'react';
import UserContext from '../../UserContext';
import Router from 'next/router';
import Link from 'next/link'
import moment from 'moment'
import BarChartIncome from '../../components/BarChartIncome'
import BarChartExpense from '../../components/BarChartExpense'
import Head from 'next/head'

import AppHelper from '../../app-helper'



export default function record({ usersData }){
	let token = ""
	
	const [records, setRecords] = useState([])
	const [searchBar, setSearchBar] = useState("")
	const [typeFilter, setTypeFilter] = useState("")
	const [recordsList, setRecordsList] = useState("")

	const { user, setUser } = useContext(UserContext)

	useEffect(() => {
		token = localStorage.getItem('token')

		fetch(`https://patternnn-budget-tracker.herokuapp.com/api/users/get-most-recent-records`, {
			method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				userId: user.userId
            })
		})
		.then(res => res.json())
		.then(data => {
			setRecordsList(data.map(data => {
				// console.log(data)
				return (
					<div className="records-card my-2">
						<ListGroup.Item key={data._id}>
							<div className="record-title">
								<b>{data.description}</b>
							</div>
							{data.type}({data.categoryName})
							<br/>
							{data.type === "Income"
							? 
								<div className="text-success record-card">
									+ {data.amount}
									<br/>
									<b>+ {data.balanceAfterTransaction}</b>
								</div>
							
							:	<div className="text-danger record-card">
									- {data.amount}
									<br/>
									<b>- {data.balanceAfterTransaction}</b>
								</div>
			
							}
							
							<br/>
							{moment(data.dateAdded).format('MMM Do YY')}
						</ListGroup.Item>
					</div>
				)
			}))
		})

		
	}, [])
	

	

	useEffect(() => {
		token = localStorage.getItem('token')

		fetch(`https://patternnn-budget-tracker.herokuapp.com/api/users/search-record`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify({
				userId: user.userId,
				searchKeyword: searchBar,
				searchType: typeFilter
            })
        })
        .then(res => res.json())
        .then(data => {
			console.log(data)
			setRecordsList(data.map(data => {
				return (
					<div className="records-card my-2">
						<ListGroup.Item key={data._id}>
							<div className="record-title">
								<b>{data.description}</b>
							</div>
							{data.type}({data.categoryName})
							<br/>
							{data.type === "Income"
							? 
								<div className="text-success record-card">
									+ {data.amount}
									<br/>
									<b>+ {data.balanceAfterTransaction}</b>
								</div>
							
							:	<div className="text-danger record-card">
									- {data.amount}
									<br/>
									<b>- {data.balanceAfterTransaction}</b>
								</div>
			
							}
							
							<br/>
							{moment(data.dateAdded).format('MMM Do YY')}
						</ListGroup.Item>
					</div>
				)
			}))
        })
	}, [searchBar, typeFilter])
	
	
	

	
	
    return (
		<React.Fragment>
			<Head>
                <title>PATTERNNN - Records</title>
            </Head>
			<div className="records-display container">
				<Link href="/records/new">
					<Button variant="outline-danger" className="login-form-button">
						<a role="button">Add a Record</a>
					</Button>
				</Link>
				
				<Form.Group controlId="recordDesc">
						<Form.Control type="text" placeholder="Search record" value={searchBar} onChange={e => setSearchBar(e.target.value)} required/>
					</Form.Group>
				<ListGroup>
				<Form.Group controlId="categoryType">
					<Form.Control as="select" type="text" defaultValue="All" onClick={e => setTypeFilter(e.target.value)} required>
						<option value="All">All</option>
						<option value="Income">Income</option>
						<option value="Expense">Expense</option>
					</Form.Control>
				</Form.Group>
					<div className="">
						{recordsList}
					</div>
				</ListGroup>
			</div>
		</React.Fragment>
    )
}

