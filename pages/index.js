import { useState, useContext } from 'react';
import { Card, Row, Col, Button } from 'react-bootstrap'
import { GoogleLogin } from 'react-google-login'
import Router from 'next/router'
import Swal from 'sweetalert2'

import UserContext from '../UserContext';
import AppHelper from '../app-helper'
import View from '../components/View'
import Head from 'next/head'

import Link from 'next/link'
// import video1 from '../public/landing-page-vid.mp4'
// import video2 from '../public/landing-page-vid2.mp4'
// import video3 from '../public/landing-page-vid3.mp4'
// import video4 from '../public/landing-page-vid4.mp4'

export default function index() {

    return (
        <React.Fragment>
            <Head>
                <title>PATTERNNN - Budget Tracker</title>
            </Head>
            {/* LANDING SECTION 1 */}
            <section id="landing">
                <div class="landing-container">
                    <div class="landing-video">
                        <video autoPlay playsInline loop id="myVideo" >
                            <source src="./landing-page-vid.mp4" type="video/mp4"/>
                        </video>
                        <h1 class="landing-text text-lg-left">
                            Finding your<br/>Budget <img className="landing-text-img" src="/for-landing-text.png"/><img className="landing-text-img" src="/for-landing-text2.png"/>
                        </h1>
                        <Link href="/register">
                            <button type="submit" class="btn btn-outline-danger landing-button my-3 hoverable">
                            
                                Get Started
                                {/* <a className="nav-link" role="button"></a>  */}
                            </button>               
                        </Link>
                    </div>
                </div>
            </section>

            {/* LANDING SECTION 2 */}
            <section id="landing">
                <div className="landing-container">
                    <div className="landing-video landing-container2 landing-bg2">
                        <video autoPlay playsInline loop id="myVideo" >
                            <source src="./landing-page-vid2.mp4" type="video/mp4"/>
                        </video>
                        <p className="container text-center landing-desc">PATTERNNN is a budget tracker app that helps you find you design your budget allocations to help you save and spend wisely. 
                        It features customizable income and expense records with personalized categories that helps adapt to your lifestyle. 
                        You can also receive feedback from a top line analytics page that visualizes your income and expenses in chart form making it more easier
                        for you to find patterns in your budget allocations.</p>
                    </div>
                </div>
            </section>

            {/* LANDING SECTION 3 */}
            <section id="landing">
                <div className="landing-container">
                    <div className="landing-video ">
                        <video autoPlay playsInline loop id="myVideo" >
                            <source src="./landing-page-vid3.mp4" type="video/mp4"/>
                        </video>
                        <div className="row mx-auto container landing-container2">
                            <div className="col-lg-3 col-md-3 col-12 mb-3 about-icon">
                                <div>
                                    <img className="img-fluid landing-icon1" src="./landing-icon1a.png"/>
                                    {/* <img className="img-fluid landing-icon2" src="./landing-icon1b.png"/> */}
                                </div>
                                <div className="text-center icon-text">
                                    <h3 className="text-center">User-friendly</h3>
                                    <p>
                                        A simplified user-interface is fit for the complexities of a budget-tracking app. The sleek and minimal experience of the app makes it easier to use and to track budget allocations.
                                    </p>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-3 col-12 mb-3 about-icon">
                                <div>
                                    <img className="img-fluid landing-icon1" src="./landing-icon2a.png"/>
                                    {/* <img className="img-fluid landing-icon2" src="./landing-icon2b.png"/> */}
                                </div>
                                <div className="text-center icon-text">
                                    <h3 className="text-center">Personalization</h3>
                                    <p>
                                        Adding your own categories and records as you see them fit is one of the main features of the app. It adapts to your current way of living as you can easily customize your budget records.
                                    </p>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-3 col-12 mb-3 about-icon">
                                <div>
                                    <img className="img-fluid landing-icon1" src="./landing-icon3a.png"/>
                                    {/* <img className="img-fluid landing-icon2" src="./landing-icon3b.png"/> */}
                                </div>
                                <div className="text-center icon-text">
                                    <h3 className="text-center">Analytics</h3>
                                    <p>
                                        Analytical tools are used together with clean data visualization tools such as charts and graphs in order to help you find your patterns in your budget allocations.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            {/* LANDING SECTION 4 */}
            <section id="landing">
                <div className="landing-container">
                    <div className="landing-video landing-container3">
                        <video autoPlay playsInline loop id="myVideo" >
                            <source src="./landing-page-vid4.mp4" type="video/mp4"/>
                        </video>
                        <div className="download-text">
                            <h1 className="text-lg-left">
                                Download the app soon!
                            </h1>
                            <p>
                                Get the PATTERNS app on your mobile phone soon for download.
                            </p>
                        </div>
                        
                        <button type="submit" className="btn btn-outline-light my-3 hoverable download-button">
                            Download
                            {/* <a className="nav-link" role="button"></a>  */}
                        </button>

                        <img className="img-fluid download-pic" src="./landing-phone.png"/>
                        <img className="img-fluid download-pic2" src="./landing-phone2.png"/>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}
