import { useState, useEffect } from 'react';
import { Row, Col, Card, Button, ListGroup, Form } from 'react-bootstrap'
import { useContext } from 'react';
import UserContext from '../UserContext';
import Router from 'next/router';
import Link from 'next/link'
import moment from 'moment'
import BarChartIncome from '../components/BarChartIncome'
import BarChartExpense from '../components/BarChartExpense'
import LineChartBalance from '../components/LineChartBalance'
import PieChartCategory from '../components/PieChartCategory'
import Head from 'next/head'



export default function trends(){
	let token = ""
	const { user, setUser } = useContext(UserContext)

	const [incomeData, setIncomeData] = useState([])
	const [expenseData, setExpenseData] = useState([])
	const [balance, setBalance] = useState([])
	const [category, setCategory] = useState([])

	useEffect(() => {
		token = localStorage.getItem('token')

		function handleBalance(){
			fetch(`https://patternnn-budget-tracker.herokuapp.com/api/users/get-most-recent-records`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					userId: user.userId
				})
			})
			.then(res => res.json())
			.then(data => {
				
				const balance = data.map(data =>{
					return data
				})
	
				setBalance(balance)
			})
		}

		function uploadIncomeData(){
			fetch(`https://patternnn-budget-tracker.herokuapp.com/api/users/get-most-recent-records`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					userId: user.userId
				})
			})
			.then(res => res.json())
			.then(data => {
				const income = data.filter(function (data){
					return data.type === "Income"
				})
		
				setIncomeData(income)
			})
		}
	
		
	
		function uploadExpenseData(){
			fetch(`https://patternnn-budget-tracker.herokuapp.com/api/users/get-most-recent-records`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					userId: user.userId
				})
			})
			.then(res => res.json())
			.then(data => {
				const expense = data.filter(function (data){
					return data.type === "Expense"
				})
		
				setExpenseData(expense)
			})
		}

		function handleCategory(){
			fetch(`https://patternnn-budget-tracker.herokuapp.com/api/users/get-most-recent-records`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					userId: user.userId
				})
			})
			.then(res => res.json())
			.then(data => {
				
				const categoryName = data.map(data =>{
					return data
				})
	
				setCategory(categoryName)
			})
		}

		handleBalance()
		uploadIncomeData()
		uploadExpenseData()
		handleCategory()
	},[user])
	

	
	
    return (
		<React.Fragment>
			<Head>
                <title>PATTERNNN - Analytics</title>
            </Head>
			<div className="container">
				<br/>
				<br/>
				<BarChartIncome rawData={incomeData} className="my-5"/>
				<hr className="my-5"/>
				<BarChartExpense rawData={expenseData} className="my-5"/>
				<hr className="my-5"/>
				<LineChartBalance rawData={balance} className="my-5"/>
				<hr className="my-5"/>
				<PieChartCategory rawData={category} className="my-5"/>
				<br/>
				<br/>
				<br/>
				<br/>
			</div>
		</React.Fragment>
    )
}

