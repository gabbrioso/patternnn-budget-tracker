import { useContext } from 'react';
import UserContext from '../UserContext';
import Router from 'next/router';
import { Row, Col, Card, Button } from 'react-bootstrap'
import View from '../components/View'
import AppHelper from '../app-helper'
import Head from 'next/head'

// let token = localStorage.getItem('token')
// let name = ''

// if(token){
// 	const options = {
// 		headers: { Authorization: `Bearer ${token}` } 
// 	}

// 	fetch(`${ AppHelper.API_URL }/users/details`, options).then(AppHelper.toJSON).then(data => {
// 		console.log(data)
// 		name = data.name
// 		// setUser({ email: data.email, isAdmin: data.isAdmin })
// 	})
// }



export default function logout(){
	
	const {unsetUser, setUser} = useContext(UserContext)

	function logoutClick(){
		unsetUser();
		Router.push('/login')
	}

	return (
		<React.Fragment>
			<Head>
                <title>PATTERNNN - Logout</title>
            </Head>
			<View title={ 'Logout' }>
				<Row className="justify-content-center logout-button">
					<Col xs md="6">
						<h3 className="text-center mb-4">Are you sure you want to logout?</h3>
						<Button variant="outline-danger" type="submit" className="login-form-button" onClick={ logoutClick } block>Logout</Button>
					</Col>
				</Row>
			</View>
		</React.Fragment>
		
	)
}