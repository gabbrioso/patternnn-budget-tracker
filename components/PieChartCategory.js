import { useState, useEffect } from 'react'
import {Pie} from 'react-chartjs-2'
import { colorRandomizer } from '../helpers'

export default function PieChart({ rawData }) {
    const [category, setCategory] = useState([])
    const [amount, setAmount] = useState([])
    const [bgColors, setBgColors] = useState([])

    useEffect(() => {
        if(rawData.length > 0){
            //segragate brands and sales into their respective states for visualization purposes
            setCategory(rawData.map(element => element.categoryName))
            setAmount(rawData.map(element => element.amount))
            //randomize background colors for every section of our pie chart using our custom-made randomizer
            setBgColors(rawData.map(() => `#${colorRandomizer()}`))
        }
    }, [rawData])

    //prepare the data to be given to the react-chart-js-2 pie chart component
    const data = {
        labels: category,
        datasets: [{
            data: amount,
            backgroundColor: bgColors,
            hoverBackgroundColor: 'rgba(231,15,110,1.0)'
        }]
    }

    //pass in the data to the pie chart component
    return (
        <React.Fragment>
            <h3 className="text-center register-text">Category Breakdown</h3>
            <Pie data={data} />
        </React.Fragment>
    )
}
