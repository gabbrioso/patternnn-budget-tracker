import { useState, useEffect } from 'react'
import { Bar } from 'react-chartjs-2'
import moment from 'moment'

export default function BarChart({rawData}){
    const [months, setMonths] = useState([])
    const [monthlyExpense, setMonthlyIncome] = useState([])

    useEffect(() => {
        if(rawData.length > 0){

            let tempMonths = []
            
            rawData.forEach(element => {
                if(!tempMonths.find(month => month === moment(element.dateAdded).format("MMMM"))){
                    tempMonths.push(moment(element.dateAdded).format("MMMM"))
                }    
            })

            const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

            tempMonths.sort((a, b) => {
                if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){
                    return monthsRef.indexOf(a) - monthsRef.indexOf(b)
                }
            })

            setMonths(tempMonths)
        }
    }, [rawData])

    useEffect(() => {
        setMonthlyIncome(months.map(month => {
            let expense = 0
            rawData.forEach(element => {
                // console.log(moment(element.dateAdded).format('MMMM'))
                if(moment(element.dateAdded).format('MMMM') === month){
                    expense = expense + parseInt(element.amount)
                }
            })
            return expense
        }))
    }, [months])

    const data = {
        labels: months,
        datasets: [
            {
                label: 'Monthly Expense for the Year 2020',
                backgroundColor: 'rgba(255,99,132,0.2)',
                borderColor: 'rgba(255,99,132,1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                hoverBorderColor: 'rgba(255,99,132,1)',
                data: monthlyExpense
            }
        ]
    }

    return(
        <React.Fragment>
            <h3 className="text-center register-text">Monthly Expenses</h3>
            <Bar data={data}/>
        </React.Fragment>
    )
}